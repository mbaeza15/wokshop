'use strict';

const express = require('express');
const asyncify = require('express-asyncify');

const router = asyncify(express.Router());

router.get('/days', async (req, res) => {
  let body = req.body;

  if (!body) {
    console.log('Error al ingresar los valores');
  }

  return res.json({
    day: body.day,
    approve: true,
    success: true,
  });
});

router.post('/days', async (req, res) => {
  console.log('Se agregó un dia');
  return res.json({
    success: true,
  });
});

router.delete('/days', async (req, res) => {
  console.log('Se agregó un dia');
  return res.json({
    success: true,
  });
});

router.put('/days', async (req, res) => {
  console.log('Se agregó un dia');
  return res.json({
    success: true,
  });
});

module.exports = router;
