/* eslint-disable require-jsdoc */

'use strict';

const http = require('http');
const express = require('express');
const asyncify = require('express-asyncify');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config/config');
const mongoose = require('mongoose');


// Se inicializa servidor web
const app = asyncify(express());
const expressServer = http.createServer(app);

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api/v1', require('./router/days-api'));


// Se agrega handler global de excepciones
app.use(function(err, req, res, next) {
  logger.error('Error de aplicacion: ', err);
  let error;
  if (err.status) {
    res.status(err.status);
    error = {
      status: err.status,
      success: false,
      code: err.code,
      message: err.message,
    };
  } else {
    res.status(500);
    error = {
      success: false,
      status: Exception.GeneralExceptionCode,
      code: 500,
      message: err.message,
    };
  }
  return res.json(error);
});

mongoose.connect(
    config.database.connectionString,
    {useNewUrlParser: true, autoReconnect: true})
    .then((res, err) => {
      if (err) {
        throw new Exception(
            `Error to connect`,
            500,
            Exception.Codes.DbExceptionCode
        );
      }
    });

expressServer.listen(config.protocol.webPort, () => {
  console.log(`web api listening on port ${config.protocol.webPort}`);
});

module.exports = expressServer;
