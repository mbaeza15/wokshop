
'use strict';

const mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL || 'mongodb://root:example@mongo-db:27017/MfaDB?authSource=admin';

module.exports = {
  /* Configuraciones de puertos */
  protocol: {
    webPort: process.env.LOGIN_PORT || 4602,
  },

  database: {
    connectionString: mongoURL,
  },
};

