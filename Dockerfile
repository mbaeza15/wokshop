# FROM registry.access.redhat.com/rhoar-nodejs/nodejs-10
FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

# RUN npm install
RUN npm install -s --only=production

# Bundle app source
COPY . .

EXPOSE 4602

CMD [ "npm", "start" ]

